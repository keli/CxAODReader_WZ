#!/usr/bin/python

import os, math, sys, string, ROOT
import hashlib
from operator import itemgetter
ROOT.gROOT.SetBatch(True)

# Script for extracting initial number of events and sum of weights
# from CxAODs, created from xAODs or DxAODs.
# The current run directory is searched for the CxAODs.
# Alternatively a sampleListFile can be given.

total = len(sys.argv)
# number of arguments plus 1
 
if total<3:
  print "You need some arguments, will ABORT!"
  print "Usage:",sys.argv[0],"countNumberInDxAOD validateLargeFiles"
  print "Usage:",sys.argv[0],"1                  0                   (to compare with AMI)"
  print "Usage:",sys.argv[0],"0                  0                   (to create yields on eos)"
  assert(False)
# done if

print sys.argv[1]
print sys.argv[2] 
 


# inputs
countNumberInDxAOD=bool(int(sys.argv[1])) # for checking DxAOD yields (replaces event counts from 'out' to 'in')
validateLargeFiles=bool(int(sys.argv[2])) # validate of the CollectionTree additionally for large files (slow)

print type(countNumberInDxAOD),countNumberInDxAOD
print type(validateLargeFiles),validateLargeFiles

out_file_md5 = "undefined"
invalid_files = []

def registerInvalidFile(message, file):
  print "ERROR:", message, "Skipping file", file
  global invalid_files
  invalid_files += [file]

def main(argv):

  sampleListFile = ""
  if len(argv) == 2:
    print "Scanning current directory for root files and trying to extract yields..."
  elif len(argv) == 4:
    sampleListFile = argv[3]
    print "Trying to extract yields from files listed in '" + sampleListFile + "'..."
  else:
    print "Usage: python $ROOTCOREBIN/../FrameworkExe/scripts/count_Nentry_SumOfWeight.py [sampleListFile]"
    return
  
  nrj = [
    "13TeV",
    ]
  
  counts = {}
  
  # prepare counter
  for i_nrj in nrj :
    counts[i_nrj] = []
  
  # check for valid file list file
  if sampleListFile is not "" and not os.path.isfile(sampleListFile):
    print "ERROR: File '" + sampleListFile + "' does not exist! Exiting."
    return
  
  # check for valid output directory
  yieldDir = "./CxAODOperation/data/"
  copyFilesMD5 = False
  if sampleListFile is not "":
    if os.path.isdir(yieldDir):
      copyFilesMD5 = True
    else:
      print "WARNING: Directory '" + yieldDir + "' does not exist, can't copy output files!"
  
  # copy file by appending its md5sum (first 10 characters) to the name
  md5sum = ""
  global out_file_md5
  if copyFilesMD5:
    md5sum = hashlib.md5(open(sampleListFile, 'rb').read()).hexdigest()
    if len(md5sum) > 10:
      md5sum = md5sum[:10]
    out_file_md5 = yieldDir + 'yields.txt.' + md5sum
    if os.path.isfile(out_file_md5):
      print "File '" + out_file_md5 + "' exists already, skipping yield counting."
      return
  
  # compile file list
  fileList = []
  if sampleListFile is "":
    for subdir, dirs, files in os.walk(".", followlinks=True) :
      for file in files :
        fileList.append(os.path.join(subdir, file))
        #print os.path.join(subdir, file)
  else:
    files = open(sampleListFile)
    for file in files :
      fileList.append(file.rstrip())
      #print file.rstrip()
    
  
  # loop over root files and count
  for my_file in fileList :

    # root file?
    if not ".root" in my_file :
      print "INFO: Skipping non-root file", my_file
      continue

    # energy
    curr_nrj = "unknown"
    for i_nrj in nrj :
      if i_nrj in my_file :
        curr_nrj = i_nrj
    if "unknown" in curr_nrj :
      print "WARNING: Energy not defined, skipping file", my_file
      continue

    # mc_channel_number
    start = string.find(my_file, curr_nrj + ".") + len(curr_nrj) + 1
    end = string.find(my_file, ".", start)
    if start < 0 or end < 0 or end - start < 2 :
      print "WARNING: MC channel number not defined, skipping file", my_file
      continue
    mc_channel_number = my_file[start : end]

    # sample_name
    start = end + 1
    end = string.find(my_file, ".", start)
    sample_name = "unknown"
    if start < 0 or end < 0 or end - start < 2 :
      print "WARNING: Unknown sample name for file", my_file
    sample_name = my_file[start : end]

    # read file and event count histogram
    file = ROOT.TFile.Open(my_file,"read")
    if not file:
      registerInvalidFile("Cannot read file!", my_file)
      continue
    if file.IsZombie():
      registerInvalidFile("Zombie file, probably broken!", my_file)
      continue
    h = file.Get("MetaData_EventCount")
    if not h:
      registerInvalidFile("MetaData_EventCount not found!", my_file)
      continue

    # check for valid CollectionTree
    if file.GetSize() < 1e6 or validateLargeFiles:
      tree = file.Get("CollectionTree")
      if not tree:
        registerInvalidFile("CollectionTree not found!", my_file)
        continue
      if tree.GetEntries() > 0 and tree.GetEntry(0) <= 0:
        registerInvalidFile("Invalid CollectionTree!", my_file)
        continue

    # count from DxAOD metadata
    n_entries = h.GetBinContent(1)
    n_selout_entries = h.GetBinContent(3)
    if countNumberInDxAOD:
      n_selout_entries = h.GetBinContent(2)
    n_sum = h.GetBinContent(4)

    # count from processed events in CxAODMaker
    if n_entries == 0 :
      n_entries = h.GetBinContent(7)
      n_sum = h.GetBinContent(8)
    if n_entries == 0 :
      print "WARNING: Zero events for file", my_file

    print "Have", curr_nrj, mc_channel_number, n_entries, n_selout_entries, n_sum, sample_name, my_file

    # find counter
    count = counts[curr_nrj]
    counter = ["unknown", 0, 0, 0, "unknown"]
    for i_count in count :
      if mc_channel_number == i_count[0] :
        counter = i_count
    if "unknown" in counter[0] :
      counter = [mc_channel_number, 0, 0, 0, sample_name]
      count.append(counter)



    counter[1] += n_entries
    counter[2] += n_selout_entries 
    counter[3] += n_sum 

    file.Close()

  if len(invalid_files) > 0:
    print "\nERROR: Found invalid files! Exiting. You can delete the files with:\n"
    print "rm \\"
    for fileName in invalid_files:
      print fileName, "\\"
    return

  # write list
  for i_nrj in nrj :
    # pick the good table
    count = counts[i_nrj]
    if len(count) is 0:
      print "INFO: No yields found for " + i_nrj + ". Skipping file writing."
      continue
    # sort by mc_channel_number
    count = sorted(count, key=itemgetter(0))
    # define file
    extrafiletag = ''
    if countNumberInDxAOD:
      extrafiletag = '_DxAOD'
    out_file_name = './yields.'+i_nrj+extrafiletag+'_sorted.txt'
    print "Writing", out_file_name
    out_file = open(out_file_name, 'w')
    # write
    for i_count in count :
      line="%-15s %-15.0f %-15.0f %-15f %-15s" % (i_count[0], i_count[1], i_count[2], i_count[3], i_count[4])
      out_file.write(line+"\n")
    out_file.close()
  
  # copy yield file by appending the md5sum of input files
  if copyFilesMD5:
    # check which energy
    # TODO should energy be removed from this script completely?
    curr_nrj = ""
    for i_nrj in nrj :
      if len(counts[i_nrj]) > 0:
        if curr_nrj != "":
          print "ERROR: Found multiple energies in file list, can't copy output file!"
          return
        curr_nrj = i_nrj
    if curr_nrj is "":
      return
    out_file_sort = './yields.'+curr_nrj+'_sorted.txt'
    print "Copying file '" + out_file_sort + "' to '" + out_file_md5 + "'..."
    os.system("cp " + out_file_sort + " " + out_file_md5)
  
    sampleListFile_md5 = yieldDir + sampleListFile + "." + md5sum
    print "Copying file '" + sampleListFile + "' to '" + sampleListFile_md5 + "'..."
    os.system("cp " + sampleListFile + " " + sampleListFile_md5)
  
if __name__ == "__main__":
   main(sys.argv[1:])
