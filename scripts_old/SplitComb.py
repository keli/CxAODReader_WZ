import ROOT
import os,sys


if len(sys.argv)!= 3:
    exit

iFile=sys.argv[1]
NChan=int(sys.argv[2])
fType=str(sys.argv[3])


rFile = ROOT.TFile(iFile,"read")


oFile = [ ROOT.TFile(iFile.replace(".root","")+"_"+str(fType)+"_"+str(chan)+".root","recreate") for chan in range(0,NChan+1)]



rList = rFile.GetListOfKeys()
for key in rList:

    dName = key.GetName()
    Dir=key.ReadObj()

    oDir = []
    for chan in range(0,NChan+1):
        oDir.append(oFile[chan].mkdir(dName))


    Dir.cd()
    lDir = Dir.GetListOfKeys()

    for key2 in lDir:
        Dir.cd()
        Name = key2.GetName()
        Object = key2.ReadObj()
        Name = Name.split("_")


        if not "TH1" in str(key2.GetClassName()):
            continue

        if ("QCD" in Name[0] or "NoBL" in Name[0] or "CF" in Name[0] or "cutflow" in Name[0]):
            continue


        Type,MC,Chan = Name[0:3]

        if not Type == fType: continue

        if Chan == "Inc":
            Chan=NChan
        else:
            Chan=int(Chan)

        oDir[Chan].cd()
        nObject = Object.Clone()
        nObject.SetName(MC)
        nObject.Print()
        nObject.Write()

        # nObject.Delete()
        # Object.Delete()
   
    Dir.cd()
    for chan in range(0,NChan+1):
        oDir[chan].Write()
        #oDir[chan].Close()
    #Dir.Delete()

for chan in range(0,NChan+1):
    oFile[chan].Write()
