
arr=($1/*)

DSIDs=()

# iterate through array using a counter
for ((i=0; i<${#arr[@]}; i++)); do
    #do something to each element of array
    name=${arr[$i]}
    nametop=${name##*mc16_13TeV.}
    #echo $nametop
    dsid=${nametop%.*.*.*.*.root}
    if [[ " ${DSIDs[*]} " == *"${dsid}"* ]]; then
	echo "arr contains d"
    else
	DSIDs+=($dsid)	
    fi


done;

echo looping

for DSID in "${DSIDs[@]}"; do
    echo $DSID
    mkdir user.isiral.mc16_13TeV.$DSID.ISMET._CxAOD.root/; 
    mv $1/mc16_13TeV.$DSID* user.isiral.mc16_13TeV.$DSID.ISMET._CxAOD.root/.; 
    cd user.isiral.mc16_13TeV.$DSID.ISMET._CxAOD.root/; 
    for file in *.root; do 
    	mv "$file" "$(basename "$file" .root)._CxAOD.root"; 
    done; 
    cd -
done;