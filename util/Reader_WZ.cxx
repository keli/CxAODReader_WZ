// xAOD headers
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"

// event loop headers
#include "EventLoop/CondorDriver.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/Driver.h"
#include "EventLoop/Job.h"
#include "EventLoop/LSFDriver.h"
#include "EventLoop/SlurmDriver.h"
#include "EventLoop/TorqueDriver.h"

// ConfigStore parses the framework configuration file
#include "CxAODTools/ConfigStore.h"

// SampleHandler headers
#include "SampleHandler/DiskListEOS.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"

// Root headers
#include <TFile.h>
#include <TPython.h>
#include <TSystem.h>

// std library headers
#include <stdlib.h>
#include <fstream>
#include <vector>

// WWW Reader
#include "CxAODReader_WZ/CxAODReader_WZ.h"

// TO RUN ON LYON BATCH
#include "EventLoop/GEDriver.h"
#include "SampleHandler/ToolsSplit.h"

void tag(SH::SampleHandler& sh, const std::string& tag);

int main(int argc, char* argv[]) {
  // TODO: replace this with command line parser and add functionality to pass all top level comands
  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  std::string configPath = "data/CxAODReader_WZ/framework-read.cfg";
  if (argc > 1) submitDir = argv[1];
  if (argc > 2) configPath = argv[2];

  // read run config
  static ConfigStore* config = ConfigStore::createStore(configPath);  // parse configuration file

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  std::string dataset_dir = config->get<std::string>("dataset_dir");
  config->getif<std::string>("submitDir", submitDir);

  // create the sample handler
  SH::SampleHandler sampleHandler;
  std::cout << "Scanning samples:" << std::endl;
  const char* inputFilePath = gSystem->ExpandPathName(dataset_dir.c_str());
  SH::ScanDir().samplePattern("*CxAOD.root").scan(sampleHandler, inputFilePath);

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sampleHandler.setMetaString("nc_tree", "CollectionTree");

  // Print what we found:
  std::cout << "Printing sample handler contents:" << std::endl;
  sampleHandler.print();

  // generate yield file from input CxAODs
  std::string yieldFile = "";
  bool generateYieldFile = false;
  config->getif<bool>("generateYieldFile", generateYieldFile);

  if (generateYieldFile) {
    // auto generate the yield file
    std::cout << "Auto Generating the Yield File: " << std::endl;
    std::string command = "pushd ";
    command += inputFilePath;
    command += " && python $WorkDir_DIR/../../source/CxAODOperations_WWW/scripts/count_Nentry_SumOfWeight.py 0 0 1 ";  // this is stupid
    command += " && popd";
    system(command.c_str());
    yieldFile += inputFilePath;
    yieldFile += "/yields.13TeV_sorted.txt";
  } else {
    // use the generated yield file
    std::cout << "Using the Yield File in the Config File: " << std::endl;
    config->getif<string>("yieldFile", yieldFile);
    yieldFile = gSystem->ExpandPathName(yieldFile.c_str());
  }
  std::cout << "Location of the yield file: " << yieldFile << std::endl;

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler(sampleHandler);
  // job.useXAOD();

  // create algorithm, set job options, maniplate members and add our analysis
  // to the job:
  AnalysisReader* algorithm = nullptr;

  std::string analysis = "WZ";  // default analysis: used below
  config->getif<std::string>("analysis", analysis);

  // save analysisType and modelType for future use

  // std::string modelType = "CUT";
  // config->getif<std::string>("modelType", modelType);

  // std::string analysisType = config->get<std::string>("analysisType");

  if (analysis == "WZ") {         // what is the point of having this?
    algorithm = new CxAODReader_WZ();  // creates a CxAODReader_WZ object
  } else {
    Error("Reader:", "Bad config: analysis = %s is not supported", analysis.c_str());
    return 1;
  }
  algorithm->setConfig(config);  // pass config to analysis
  // add automatic sum of weight
  algorithm->setSumOfWeightsFile(yieldFile);

  // limit number of events to maxEvents - set in config
  job.options()->setDouble(EL::Job::optMaxEvents, config->get<int>("maxEvents"));
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EventLoop#Access_the_Data_Through_xAOD_EDM
  bool nominalOnly = false;
  config->getif<bool>("nominalOnly", nominalOnly);
  if (nominalOnly) {
    // branch access shows better performance for CxAODReader with Nominal only
    job.options()->setString(EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_branch);
  } else {
    // branch access cannot read shallow copies, so we need class access in case
    // reading systematics
    job.options()->setString(EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);
  }

  // add algorithm to job
  job.algsAdd(algorithm);

  // Number of files to submit per job
  int nFilesPerJob = 20;
  config->getif<int>("nFilesPerJob", nFilesPerJob);

  // Run the job using the local/direct driver:
  std::string driver = "direct";
  config->getif<std::string>("driver", driver);
  if (driver == "direct") {
    EL::DirectDriver* eldriver = new EL::DirectDriver;
    eldriver->submit(job, submitDir);
    // } else if (driver == "proof") {
    //   Error("hsg5framework",
    //         "The ProofDriver is no longer supported. See ATLASG-1376.");
    //   return EXIT_FAILURE;
    // } else if (driver == "LSF") {
    //   EL::LSFDriver* eldriver = new EL::LSFDriver;
    //   eldriver->options()->setString(EL::Job::optSubmitFlags, "-L /bin/bash");
    //   eldriver->shellInit =
    //       "export "
    //       "ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && "
    //       "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";
    //   job.options()->setDouble(EL::Job::optFilesPerWorker, nFilesPerJob);
    //   std::string bQueue = "1nh";
    //   config->getif<std::string>("bQueue", bQueue);
    //   job.options()->setString(EL::Job::optSubmitFlags,
    //                            ("-q " + bQueue).c_str());  // 1nh 8nm
    //   eldriver->submitOnly(job, submitDir);
  } else if (driver == "condor") {  // Spyros: add condor driver
    EL::CondorDriver* eldriver = new EL::CondorDriver;
    eldriver->shellInit =
        "export "
        "ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && "
        "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";
    job.options()->setDouble(EL::Job::optFilesPerWorker, nFilesPerJob);
    std::string condor_config = "";
    std::string condor_queue = "none";
    config->getif<std::string>("condor_queue", condor_queue);
    if (condor_queue != "none") {
      condor_config.append(("+JobFlavour = \"" + condor_queue + "\"").c_str());
    }

    // for the moment fix to run on CentOS7 machines
    // condor_config.append(" \n");
    // condor_config.append("requirements = (OpSysAndVer =?= \"CentOS7\")");
    // condor_config.append(" \n");

    std::string accountingGroup = "none";
    config->getif<std::string>("accountingGroup", accountingGroup);
    if (accountingGroup != "none") {  // What is this?
      condor_config.append(" \n");
      condor_config.append(("accounting_group = " + accountingGroup).c_str());
    }

    job.options()->setString(EL::Job::optCondorConf, condor_config);
    eldriver->submitOnly(job, submitDir);
    // } else if (driver ==
    //            "GE") {  // grid engine - Camilla (lyon batch), Felix (MPI batch)
    //   std::vector<std::string> vec_submit_flags =
    //       config->get<std::vector<std::string> >("submitFlags");
    //   std::string submit_flags("");
    //   for (auto str : vec_submit_flags) submit_flags += str + " ";
    //   EL::GEDriver* eldriver = new EL::GEDriver;
    //   job.options()->setDouble(EL::Job::optFilesPerWorker, nFilesPerJob);
    //   job.options()->setString(EL::Job::optSubmitFlags, submit_flags);
    //   eldriver->submit(job, submitDir);
    //   // eldriver->submitOnly(job, submitDir);
    // } else if (driver == "slurm") {
    //   // slurm driver (Freiburg batch)
    //   std::string slurm_account = config->get<std::string>("slurm_account");
    //   std::string slurm_partition = config->get<std::string>("slurm_partition");
    //   std::string slurm_runTime = config->get<std::string>("slurm_runTime");
    //   std::string slurm_memory = config->get<std::string>("slurm_memory");
    //   EL::SlurmDriver* eldriver = new EL::SlurmDriver;
    //   eldriver->SetJobName("ReadCxAOD");
    //   eldriver->SetAccount(slurm_account);
    //   eldriver->SetPartition(slurm_partition);
    //   eldriver->SetRunTime(slurm_runTime);
    //   eldriver->SetMemory(slurm_memory);
    //   eldriver->SetConstrain("");
    //   eldriver->submitOnly(job, submitDir);
    // } else if (driver == "torque") {
    //   EL::TorqueDriver* eldriver = new EL::TorqueDriver;
    //   eldriver->shellInit =
    //       "export "
    //       "ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && "
    //       "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";
    //   std::string stbc_queue = "generic";
    //   config->getif<std::string>("stbc_queue", stbc_queue);
    //   job.options()->setString(
    //       EL::Job::optSubmitFlags,
    //       ("-q " + stbc_queue).c_str());  // generic queue default - 24h walltime
    //   eldriver->submitOnly(job, submitDir);
  } else {
    Error("CxAODReader_WZ", "Unknown driver '%s'", driver.c_str());
    return 1;
  }
  return 0;
}
