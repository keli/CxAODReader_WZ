//#include <EventLoop/Worker.h>

//#include "TSystem.h"

#include "CxAODReader_WZ/DBEvtSelection.h"

#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODJet/Jet.h"
#include "xAODMuon/Muon.h"
//todo: add tau, photon?
//#include "xAODEventInfo/EventInfo.h"

#include "CxAODTools/CommonProperties.h"
#include "CxAODTools_WZ/CommonProperties_WWW.h"

#include <iostream>
#include "TVector2.h"

// rename these and merge into one function that takes the base clase of all of these
bool compare_el_pt(const xAOD::Electron* jet1, const xAOD::Electron* jet2) { return jet1->pt() > jet2->pt(); }

bool compare_mu_pt(const xAOD::Muon* jet1, const xAOD::Muon* jet2) { return jet1->pt() > jet2->pt(); }

bool compare_ta_pt(const xAOD::TauJet* jet1, const xAOD::TauJet* jet2) { return jet1->pt() > jet2->pt(); }

// Constructor
DBEvtSelection::DBEvtSelection()  //ConfigStore* config)
    : EventSelection() {}

void DBEvtSelection::clearResult() {
  //change so we are clearing the items in the DBResult struct
  m_result.pass = false;
  // m_result.signalJets.clear();
  // m_result.forwardJets.clear();
  m_result.smallJets.clear();
  // m_result.largeJets.clear();
  // m_result.trackJets.clear();
  m_result.el.clear();
  m_result.fwdel.clear();
  m_result.mu.clear();
  m_result.ta.clear();
  m_result.met = nullptr;
  m_result.mpt = nullptr;
  // m_result.TrackPart.clear();
  // m_result.Photons.clear();
  // m_result.truthParticles.clear();
}

int DBEvtSelection::doWWWLeptonPreSelection(const xAOD::ElectronContainer* electrons, const xAOD::ElectronContainer* forwardelectrons,
                                            const xAOD::MuonContainer* muons, const xAOD::TauJetContainer* taus,
                                            std::vector<const xAOD::Electron*>& el, std::vector<const xAOD::Electron*>& fwdel,
                                            std::vector<const xAOD::Muon*>& mu, std::vector<const xAOD::TauJet*>& ta) {
  // mu, el, and tau are passed by reference
  // this function returns an int but also fills
  // these vectors
  el.clear();
  fwdel.clear();
  mu.clear();
  ta.clear();
  int nelecs = 0;
  int nmuons = 0;
  int nelecsSig = 0;
  int nmuonsSig = 0;

  if (forwardelectrons) {
    //std::cout<< "!!! has forwarelectron"<<std::endl;
    for (unsigned int iElec = 0; iElec < forwardelectrons->size(); ++iElec) {
      const xAOD::Electron* elec = forwardelectrons->at(iElec);
      fwdel.push_back(elec);
    }
  }

  for (unsigned int iElec = 0; iElec < electrons->size(); ++iElec) {
    const xAOD::Electron* elec = electrons->at(iElec);
    if (!Props::passOR.get(elec)) {
      //std::cout<< "!!!! not pass OR Electron !!!"<< std::endl;
      continue;
    }
    //if(elec->pt() < 21e3) std::cout<< "!!!! Veto Electron !!!"<< elec->pt() << std::endl;
// for WZ VBS
    if (Props::isVetoElectron.get(elec) ){//|| Props::isNoBLElectron.get(elec)) {
      el.push_back(elec);
    }

    //if( fabs( Props::ElClusterEta.get(elec) ) < 1.52 && fabs( Props::ElClusterEta.get(elec) )>1.37 ) continue;

    if (Props::isZElectron.get(elec)){// || Props::isNoBLElectron.get(elec)) {
      nelecs++;
    }
    //if (DBProps::isVVSignalElectron.get(elec)) nelecsSig++;
    // if ( fabs(Props::ElClusterEta.get( elec ) ) < 1.52 && fabs( Props::ElClusterEta.get( elec ) ) > 1.37 ) {
    //    continue;
    // }

    if (Props::isWElectron.get(elec)) {
      nelecsSig++;
    }
  }
  std::sort(el.begin(), el.end(), compare_el_pt);

  for (unsigned int iMuon = 0; iMuon < muons->size(); ++iMuon) {
    const xAOD::Muon* muon = muons->at(iMuon);
    //if ( !Props::passOR.get(muon) ) continue;
    if (!Props::passOR.get(muon)) {
      //std::cout<< "!!!! not pass OR muon !!!"<< std::endl;
      continue;
    }
    //if(muon->pt() < 21e3) std::cout<< "!!!! Veto Muon !!!"<< muon->pt() << std::endl;

    if (Props::isVetoMuon.get(muon)) {
      mu.push_back(muon);
    }

    if (Props::isZMuon.get(muon)) {
      nmuons++;
    }

    if (Props::isWMuon.get(muon)) nmuonsSig++;
  }
  std::sort(mu.begin(), mu.end(), compare_mu_pt);

  for (unsigned int iTau = 0; iTau < taus->size(); ++iTau) {
    const xAOD::TauJet* tau = taus->at(iTau);
    ta.push_back(tau);
  }
  std::sort(ta.begin(), ta.end(), compare_ta_pt);

  // if ( nelecs + nmuons == 0 ) {
  //   return 0;
  // } else if ( nelecs + nmuons == 1 && nelecsSig + nmuonsSig == 1 ) {
  //   return 1;
  // } else if ( nelecs + nmuons >= 2 && nelecsSig + nmuonsSig >= 1 ) {
  //   return 2;
  // }

  if (el.size() + mu.size() >= 2 ) return 2;
  //if (nelecs + nmuons >= 2 && nelecsSig + nmuonsSig >= 1) return 2;

  return -1;
}

// bool DBEvtSelection::passKinematics() {
//   // MJ cuts, like MET / MPT etc...
//   // my advice is to add in passKinematics() prototype all the stuff that
//   // doesn't need to be put in the Result struct, like MPT

//   return true;
// }

// bool DBEvtSelection::passJetCleaning(const xAOD::JetContainer* jets) {
//   bool result = true;
//   for (const auto j : *jets) {
//     if (!Props::isSignalJet.get(j)) {
//       result = false;
//       break;
//     }
//   }
//   return result;
// }

// bool DBEvtSelection::doJetCleaning(const xAOD::JetContainer* jets) {
//   bool found_badjet = false;
//   for (const xAOD::Jet * jet : *jets) {
//     //if (!DBProps::isBadJet.get(jet)) continue;
//     if ( Props::goodJet.get(jet)) continue;
//     if(jet->pt() > 50e3) {
//         found_badjet = true;
//     } else if (jet->pt() > 20e3 && (fabs(jet->eta()) > 2.4 || Props::Jvt.get(jet)>0.64)) {
//         found_badjet = true;
//     }
//   }
//   return !found_badjet;
// }
