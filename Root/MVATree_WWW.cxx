#include "CxAODReader_WZ/MVATree_WWW.h"

MVATree_WWW::MVATree_WWW(bool persistent, bool readMVA, int analysisType, EL::IWorker *tmpwk, std::vector<std::string> variations,
                         std::vector<HistSvc::WeightSyst> *weightSysts, bool nominalOnly)
    : MVATree(persistent, readMVA, tmpwk, variations, nominalOnly), m_analysisType(analysisType), m_weightSysts(weightSysts) {
  if (nominalOnly) {
    variations = {"Nominal"};
  };
  for (std::string varName : variations) {
    SetVariation(varName);
    SetBranches();
  }
  for (unsigned int i = 0; !nominalOnly && i < m_weightSysts->size(); i++) {
    TFile *tmpF = tmpwk->getOutputFile("MVATree");
    m_treeMap[m_weightSysts->at(i).name] = new TTree(m_weightSysts->at(i).name.c_str(), m_weightSysts->at(i).name.c_str());
    m_treeMap[m_weightSysts->at(i).name]->SetDirectory(tmpF);
    SetVariation(m_weightSysts->at(i).name);
    SetBranches();
  }
}

void MVATree_WWW::SetBranches() {
  m_treeMap[m_currentVar]->Branch("runNumber", &runNumber, "runNumber/I");
  m_treeMap[m_currentVar]->Branch("eventNumber", &eventNumber, "eventNumber/I");
  m_treeMap[m_currentVar]->Branch("Nleptons", &Nleptons, "Nleptons/I");
  m_treeMap[m_currentVar]->Branch("Njets", &Njets, "Njets/I");
  m_treeMap[m_currentVar]->Branch("Nbjets", &Nbjets, "Nbjets/I");
  m_treeMap[m_currentVar]->Branch("Nbjets77", &Nbjets77, "Nbjets77/I");
  m_treeMap[m_currentVar]->Branch("Nloose", &Nloose, "Nloose/I");
  m_treeMap[m_currentVar]->Branch("Nlooseel", &Nlooseel, "Nlooseel/I");
  m_treeMap[m_currentVar]->Branch("NNoBLel", &NNoBLel, "NNoBLel/I");
  m_treeMap[m_currentVar]->Branch("Nloosemu", &Nloosemu, "Nloosemu/I");
  m_treeMap[m_currentVar]->Branch("NAllMu", &NAllMu, "NAllMu/I");
  m_treeMap[m_currentVar]->Branch("NAllEl", &NAllEl, "NAllEl/I");
  m_treeMap[m_currentVar]->Branch("NAllTa", &NAllTa, "NAllTa/I");
  m_treeMap[m_currentVar]->Branch("Nel", &Nel, "Nel/I");
  m_treeMap[m_currentVar]->Branch("Nmu", &Nmu, "Nmu/I");

  m_treeMap[m_currentVar]->Branch("TotalQ", &TotalQ, "TotalQ/I");

  m_treeMap[m_currentVar]->Branch("met", &met, "met/F");
  m_treeMap[m_currentVar]->Branch("metSig", &metSig, "metSig/F");
  m_treeMap[m_currentVar]->Branch("metSigRatio", &metSigRatio, "metSigRatio/F");

  // m_treeMap[m_currentVar]->Branch("mpt", &mpt, "mpt/F");
  // m_treeMap[m_currentVar]->Branch("MT", &MT, "MT/F");
  // m_treeMap[m_currentVar]->Branch("MTtrack", &MTtrack, "MTtrack/F");
  m_treeMap[m_currentVar]->Branch("metPhi", &metPhi, "metPhi/F");
  // m_treeMap[m_currentVar]->Branch("mptPhi", &mptPhi, "mptPhi/F");

  m_treeMap[m_currentVar]->Branch("weight", &weight, "weight/F");
  m_treeMap[m_currentVar]->Branch("weightMC", &weightMC, "weightMC/F");
  m_treeMap[m_currentVar]->Branch("weightJVT", &weightJVT, "weightJVT/F");
  m_treeMap[m_currentVar]->Branch("weightBtag", &weightBtag, "weightBtag/F");
  m_treeMap[m_currentVar]->Branch("weightEffSF", &weightEffSF, "weightEffSF/F");
  m_treeMap[m_currentVar]->Branch("weightEffReco", &weightEffReco, "weightEffReco/F");
  m_treeMap[m_currentVar]->Branch("weightIso", &weightIso, "weightIso/F");
  m_treeMap[m_currentVar]->Branch("weightTTVA", &weightTTVA, "weightTTVA/F");
  m_treeMap[m_currentVar]->Branch("weightTrig", &weightTrig, "weightTrig/F");
  m_treeMap[m_currentVar]->Branch("weightSys", &weightSys, "weightSys/F");

  m_treeMap[m_currentVar]->Branch("QCDweight", &QCDweight, "QCDweight/F");
  m_treeMap[m_currentVar]->Branch("CFweight", &CFweight, "CFweight/F");
  m_treeMap[m_currentVar]->Branch("CFStatSys", &CFStatSys, "CFStatSys/F");
  m_treeMap[m_currentVar]->Branch("CFwBG", &CFwBG, "CFwBG/F");

  m_treeMap[m_currentVar]->Branch("Mll", &Mll, "Mll/F");
  m_treeMap[m_currentVar]->Branch("Mlll", &Mlll, "Mlll/F");
  m_treeMap[m_currentVar]->Branch("Mllee", &Mllee, "Mllee/F");
  m_treeMap[m_currentVar]->Branch("Mllmm", &Mllmm, "Mllmm/F");
  m_treeMap[m_currentVar]->Branch("MllSF", &MllSF, "MllSF/F");
  m_treeMap[m_currentVar]->Branch("mllSFOS", &mllSFOS, "mllSFOS/F");
  m_treeMap[m_currentVar]->Branch("Mll_forwardelectron", &Mll_forwardelectron, "Mll_forwardelectron/F");
  m_treeMap[m_currentVar]->Branch("forwardElPt", &forwardElPt, "forwardElPt/F");
  m_treeMap[m_currentVar]->Branch("forwardElEta", &forwardElEta, "forwardElEta/F");

  m_treeMap[m_currentVar]->Branch("mtll", &mtll, "mtll/F");
  m_treeMap[m_currentVar]->Branch("mtlll", &mtlll, "mtlll/F");

  m_treeMap[m_currentVar]->Branch("mtLep1", &mtLep1, "mtLep1/F");
  m_treeMap[m_currentVar]->Branch("mtLep2", &mtLep2, "mtLep2/F");
  m_treeMap[m_currentVar]->Branch("mtLep3", &mtLep3, "mtLep3/F");

  m_treeMap[m_currentVar]->Branch("Mjj", &Mjj, "Mjj/F");
  m_treeMap[m_currentVar]->Branch("MWWW", &MWWW, "MWWW/F");

  m_treeMap[m_currentVar]->Branch("Ptll", &Ptll, "Ptll/F");
  m_treeMap[m_currentVar]->Branch("Ptjj", &Ptjj, "Ptjj/F");
  m_treeMap[m_currentVar]->Branch("PtWWW", &PtWWW, "PtWWW/F");

  m_treeMap[m_currentVar]->Branch("Etall", &Etall, "Etall/F");
  m_treeMap[m_currentVar]->Branch("dEtall", &dEtall, "dEtall/F");
  m_treeMap[m_currentVar]->Branch("etalll", &etalll, "etalll/F");
  m_treeMap[m_currentVar]->Branch("Etajj", &Etajj, "Etajj/F");
  m_treeMap[m_currentVar]->Branch("DEtajj", &DEtajj, "DEtajj/F");
  m_treeMap[m_currentVar]->Branch("EtaWWW", &EtaWWW, "EtaWWW/F");

  m_treeMap[m_currentVar]->Branch("Phill", &Phill, "Phill/F");
  m_treeMap[m_currentVar]->Branch("dPhill", &dPhill, "dPhill/F");
  m_treeMap[m_currentVar]->Branch("philll", &philll, "philll/F");
  m_treeMap[m_currentVar]->Branch("Phijj", &Phijj, "Phijj/F");
  m_treeMap[m_currentVar]->Branch("PhiWWW", &PhiWWW, "PhiWWW/F");

  m_treeMap[m_currentVar]->Branch("SumEta", &SumEta, "SumEta/F");
  m_treeMap[m_currentVar]->Branch("SumPt", &SumPt, "SumPt/F");

  m_treeMap[m_currentVar]->Branch("Jet1Pt", &Jet1Pt, "Jet1Pt/F");
  m_treeMap[m_currentVar]->Branch("Jet1M", &Jet1M, "Jet1M/F");
  m_treeMap[m_currentVar]->Branch("Jet1E", &Jet1E, "Jet1E/F");
  m_treeMap[m_currentVar]->Branch("Jet1Eta", &Jet1Eta, "Jet1Eta/F");
  m_treeMap[m_currentVar]->Branch("Jet1Phi", &Jet1Phi, "Jet1Phi/F");

  // m_treeMap[m_currentVar]->Branch("Jet1nrMuon", &Jet1nrMuon, "Jet1nrMuon/I");
  // m_treeMap[m_currentVar]->Branch("Jet1nrEl", &Jet1nrEl, "Jet1nrEl/I");
  // m_treeMap[m_currentVar]->Branch("Jet2nrMuon", &Jet2nrMuon, "Jet2nrMuon/I");
  // m_treeMap[m_currentVar]->Branch("Jet2nrEl", &Jet2nrEl, "Jet2nrEl/I");

  // m_treeMap[m_currentVar]->Branch("Jet1NTrkPt500PV", &Jet1NTrkPt500PV, "Jet1NTrkPt500PV/I");
  // m_treeMap[m_currentVar]->Branch("Jet2NTrkPt500PV", &Jet2NTrkPt500PV, "Jet2NTrkPt500PV/I");

  // m_treeMap[m_currentVar]->Branch("Jet1NTrkPt1000PV", &Jet1NTrkPt1000PV, "Jet1NTrkPt1000PV/I");
  // m_treeMap[m_currentVar]->Branch("Jet2NTrkPt1000PV", &Jet2NTrkPt1000PV, "Jet2NTrkPt1000PV/I");

  // m_treeMap[m_currentVar]->Branch("bJet1Pt", &bJet1Pt, "bJet1Pt/F");
  // m_treeMap[m_currentVar]->Branch("bJet1M", &bJet1M, "bJet1M/F");
  // m_treeMap[m_currentVar]->Branch("bJet1E", &bJet1E, "bJet1E/F");
  // m_treeMap[m_currentVar]->Branch("bJet1Eta", &bJet1Eta, "bJet1Eta/F");
  // m_treeMap[m_currentVar]->Branch("bJet1Phi", &bJet1Phi, "bJet1Phi/F");

  m_treeMap[m_currentVar]->Branch("Jet2Pt", &Jet2Pt, "Jet2Pt/F");
  m_treeMap[m_currentVar]->Branch("Jet2M", &Jet2M, "Jet2M/F");
  m_treeMap[m_currentVar]->Branch("Jet2E", &Jet2E, "Jet2E/F");
  m_treeMap[m_currentVar]->Branch("Jet2Eta", &Jet2Eta, "Jet2Eta/F");
  m_treeMap[m_currentVar]->Branch("Jet2Phi", &Jet2Phi, "Jet2Phi/F");

  m_treeMap[m_currentVar]->Branch("vetoMuonPt", &vetoMuonPt, "vetoMuonPt/F");
  m_treeMap[m_currentVar]->Branch("vetoMuonEta", &vetoMuonEta, "vetoMuonEta/F");
  m_treeMap[m_currentVar]->Branch("vetoMuonPhi", &vetoMuonPhi, "vetoMuonPhi/F");

  m_treeMap[m_currentVar]->Branch("vetoElectronPt", &vetoElectronPt, "vetoElectronPt/F");
  m_treeMap[m_currentVar]->Branch("vetoElectronEta", &vetoElectronEta, "vetoElectronEta/F");
  m_treeMap[m_currentVar]->Branch("vetoElectronPhi", &vetoElectronPhi, "vetoElectronPhi/F");

  m_treeMap[m_currentVar]->Branch("Lep1PromptWeight", &Lep1PromptWeight, "Lep1PromptWeight/F");
  m_treeMap[m_currentVar]->Branch("Lep2PromptWeight", &Lep2PromptWeight, "Lep2PromptWeight/F");
  m_treeMap[m_currentVar]->Branch("LepPLVEl", &LepPLVEl, "LepPLVEl/F");
  m_treeMap[m_currentVar]->Branch("LepPLVMu", &LepPLVMu, "LepPLVMu/F");

  m_treeMap[m_currentVar]->Branch("Lep1ECIDSBDT", &Lep1ECIDSBDT, "Lep1ECIDSBDT/F");
  m_treeMap[m_currentVar]->Branch("Lep2ECIDSBDT", &Lep2ECIDSBDT, "Lep2ECIDSBDT/F");
  m_treeMap[m_currentVar]->Branch("LepECIDSBDT", &LepECIDSBDT, "LepECIDSBDT/F");

  m_treeMap[m_currentVar]->Branch("LepAddAmbiguity", &LepAddAmbiguity, "LepAddAmbiguity/I");

  m_treeMap[m_currentVar]->Branch("Lep1Pt", &Lep1Pt, "Lep1Pt/F");
  m_treeMap[m_currentVar]->Branch("Lep1E", &Lep1E, "Lep1E/F");
  m_treeMap[m_currentVar]->Branch("Lep1Eta", &Lep1Eta, "Lep1Eta/F");
  m_treeMap[m_currentVar]->Branch("Lep1Phi", &Lep1Phi, "Lep1Phi/F");
  m_treeMap[m_currentVar]->Branch("Lep1innerTrackPt", &Lep1innerTrackPt, "Lep1innerTrackPt/F");

  m_treeMap[m_currentVar]->Branch("Lep1Q", &Lep1Q, "Lep1Q/I");
  m_treeMap[m_currentVar]->Branch("Lep1Flav", &Lep1Flav, "Lep1Flav/I");
  m_treeMap[m_currentVar]->Branch("Lep1isLoose", &Lep1isLoose, "Lep1isLoose/O");
  m_treeMap[m_currentVar]->Branch("Lep1Author", &Lep1Author, "Lep1Author/I");
  m_treeMap[m_currentVar]->Branch("Lep1isNoBL", &Lep1isNoBL, "Lep1isNoBL/O");

  // JL: testing
  m_treeMap[m_currentVar]->Branch("Lep1isPrompt", &Lep1isPrompt, "Lep1isPrompt/I");
  m_treeMap[m_currentVar]->Branch("Lep1isFake", &Lep1isFake, "Lep1isFake/I");
  m_treeMap[m_currentVar]->Branch("Lep1truthStatus", &Lep1truthStatus, "Lep1truthStatus/I");

  m_treeMap[m_currentVar]->Branch("Lep2Pt", &Lep2Pt, "Lep2Pt/F");
  m_treeMap[m_currentVar]->Branch("Lep2E", &Lep2E, "Lep2E/F");
  m_treeMap[m_currentVar]->Branch("Lep2Eta", &Lep2Eta, "Lep2Eta/F");
  m_treeMap[m_currentVar]->Branch("Lep2Phi", &Lep2Phi, "Lep2Phi/F");
  m_treeMap[m_currentVar]->Branch("Lep2innerTrackPt", &Lep2innerTrackPt, "Lep2innerTrackPt/F");

  m_treeMap[m_currentVar]->Branch("Lep2Q", &Lep2Q, "Lep2Q/I");
  m_treeMap[m_currentVar]->Branch("Lep2Flav", &Lep2Flav, "Lep2Flav/I");
  m_treeMap[m_currentVar]->Branch("Lep2isLoose", &Lep2isLoose, "Lep2isLoose/O");
  m_treeMap[m_currentVar]->Branch("Lep2Author", &Lep2Author, "Lep2Author/I");
  m_treeMap[m_currentVar]->Branch("Lep2isNoBL", &Lep2isNoBL, "Lep2isNoBL/O");

  // JL: testing
  m_treeMap[m_currentVar]->Branch("Lep2isPrompt", &Lep2isPrompt, "Lep2isPrompt/I");
  m_treeMap[m_currentVar]->Branch("Lep2isFake", &Lep2isFake, "Lep2isFake/I");
  m_treeMap[m_currentVar]->Branch("Lep2truthStatus", &Lep2truthStatus, "Lep2truthStatus/I");

  m_treeMap[m_currentVar]->Branch("Lep3Q", &Lep3Q, "Lep3Q/I");
  m_treeMap[m_currentVar]->Branch("Lep3Flav", &Lep3Flav, "Lep3Flav/I");
  m_treeMap[m_currentVar]->Branch("Lep3isLoose", &Lep3isLoose, "Lep3isLoose/O");
  m_treeMap[m_currentVar]->Branch("Lep3Author", &Lep3Author, "Lep3Author/I");
  m_treeMap[m_currentVar]->Branch("Lep3isNoBL", &Lep3isNoBL, "Lep3isNoBL/O");

  // JL: testing
  m_treeMap[m_currentVar]->Branch("Lep3isPrompt", &Lep3isPrompt, "Lep3isPrompt/I");
  m_treeMap[m_currentVar]->Branch("Lep3isFake", &Lep3isFake, "Lep3isFake/I");
  m_treeMap[m_currentVar]->Branch("Lep3truthStatus", &Lep3truthStatus, "Lep3truthStatus/I");

  m_treeMap[m_currentVar]->Branch("LepOFQ", &LepOFQ, "LepOFQ/I");
  m_treeMap[m_currentVar]->Branch("LepOFFlav", &LepOFFlav, "LepOFFlav/I");
  m_treeMap[m_currentVar]->Branch("LepOFisLoose", &LepOFisLoose, "LepOFisLoose/O");
  m_treeMap[m_currentVar]->Branch("LepOFisNoBL", &LepOFisNoBL, "LepOFisNoBL/I");
  m_treeMap[m_currentVar]->Branch("LepOFPt", &LepOFPt, "LepOFPt/F");
  m_treeMap[m_currentVar]->Branch("LepOFE", &LepOFE, "LepOFE/F");
  m_treeMap[m_currentVar]->Branch("LepOFEta", &LepOFEta, "LepOFEta/F");
  m_treeMap[m_currentVar]->Branch("LepOFPhi", &LepOFPhi, "LepOFPhi/F");

  m_treeMap[m_currentVar]->Branch("Lep1TopoEtCone20", &Lep1TopoEtCone20, "Lep1TopoEtCone20/F");
  m_treeMap[m_currentVar]->Branch("Lep1PtCone20", &Lep1PtCone20, "Lep1PtCone20/F");
  m_treeMap[m_currentVar]->Branch("Lep1PtVarCone20", &Lep1PtVarCone20, "Lep1PtVarCone20/F");
  m_treeMap[m_currentVar]->Branch("Lep1PtVarCone30", &Lep1PtVarCone30, "Lep1PtVarCone30/F");
  m_treeMap[m_currentVar]->Branch("Lep1PtVarCone40", &Lep1PtVarCone40, "Lep1PtVarCone40/F");
  m_treeMap[m_currentVar]->Branch("Lep1PtVarCone20_TightTTVA_pt1000", &Lep1PtVarCone20_TightTTVA_pt1000,
                                  "Lep1PtVarCone20_TightTTVA_pt1000/F");
  m_treeMap[m_currentVar]->Branch("Lep1PtVarCone30_TightTTVA_pt1000", &Lep1PtVarCone30_TightTTVA_pt1000,
                                  "Lep1PtVarCone30_TightTTVA_pt1000/F");

  // m_treeMap[m_currentVar]->Branch("Lep1d0", &Lep1d0, "Lep1d0/F");

  m_treeMap[m_currentVar]->Branch("Lep2TopoEtCone20", &Lep2TopoEtCone20, "Lep2TopoEtCone20/F");
  m_treeMap[m_currentVar]->Branch("Lep2PtCone20", &Lep2PtCone20, "Lep2PtCone20/F");
  m_treeMap[m_currentVar]->Branch("Lep2PtVarCone20", &Lep2PtVarCone20, "Lep2PtVarCone20/F");
  m_treeMap[m_currentVar]->Branch("Lep2PtVarCone30", &Lep2PtVarCone30, "Lep2PtVarCone30/F");
  m_treeMap[m_currentVar]->Branch("Lep2PtVarCone40", &Lep2PtVarCone40, "Lep2PtVarCone40/F");
  m_treeMap[m_currentVar]->Branch("Lep2PtVarCone20_TightTTVA_pt1000", &Lep2PtVarCone20_TightTTVA_pt1000,
                                  "Lep2PtVarCone20_TightTTVA_pt1000/F");
  m_treeMap[m_currentVar]->Branch("Lep2PtVarCone30_TightTTVA_pt1000", &Lep2PtVarCone30_TightTTVA_pt1000,
                                  "Lep2PtVarCone30_TightTTVA_pt1000/F");
  // m_treeMap[m_currentVar]->Branch("Lep2d0", &Lep2d0, "Lep2d0/F");

  m_treeMap[m_currentVar]->Branch("Lep3Pt", &Lep3Pt, "Lep3Pt/F");
  m_treeMap[m_currentVar]->Branch("Lep3E", &Lep3E, "Lep3E/F");
  m_treeMap[m_currentVar]->Branch("Lep3Eta", &Lep3Eta, "Lep3Eta/F");
  m_treeMap[m_currentVar]->Branch("Lep3Phi", &Lep3Phi, "Lep3Phi/F");
  m_treeMap[m_currentVar]->Branch("Lep3innerTrackPt", &Lep3innerTrackPt, "Lep3innerTrackPt/F");

  m_treeMap[m_currentVar]->Branch("LooseMuPt", &LooseMuPt, "LooseMuPt/F");
  m_treeMap[m_currentVar]->Branch("LooseMuE", &LooseMuE, "LooseMuE/F");
  m_treeMap[m_currentVar]->Branch("LooseMuEta", &LooseMuEta, "LooseMuEta/F");
  m_treeMap[m_currentVar]->Branch("LooseMuPhi", &LooseMuPhi, "LooseMuPhi/F");

  m_treeMap[m_currentVar]->Branch("LooseElPt", &LooseElPt, "LooseElPt/F");
  m_treeMap[m_currentVar]->Branch("LooseElE", &LooseElE, "LooseElE/F");
  m_treeMap[m_currentVar]->Branch("LooseElEta", &LooseElEta, "LooseElEta/F");
  m_treeMap[m_currentVar]->Branch("LooseElPhi", &LooseElPhi, "LooseElPhi/F");

  m_treeMap[m_currentVar]->Branch("TauPt", &TauPt, "TauPt/F");
  m_treeMap[m_currentVar]->Branch("TauE", &TauE, "TauE/F");
  m_treeMap[m_currentVar]->Branch("TauEta", &TauEta, "TauEta/F");
  m_treeMap[m_currentVar]->Branch("TauPhi", &TauPhi, "TauPhi/F");
  m_treeMap[m_currentVar]->Branch("TaunTracks", &TaunTracks, "TaunTracks/I");
  m_treeMap[m_currentVar]->Branch("Nloosetau", &Nloosetau, "Nloosetau/I");
  m_treeMap[m_currentVar]->Branch("Nmediumtau", &Nmediumtau, "Nmediumtau/I");
  m_treeMap[m_currentVar]->Branch("Ntighttau", &Ntighttau, "Ntighttau/I");

  // m_treeMap[m_currentVar]->Branch("tLep1Pt", &tLep1Pt, "tLep1Pt/F");
  // m_treeMap[m_currentVar]->Branch("tLep1E", &tLep1E, "tLep1E/F");
  // m_treeMap[m_currentVar]->Branch("tLep1Eta", &tLep1Eta, "tLep1Eta/F");
  // m_treeMap[m_currentVar]->Branch("tLep1Phi", &tLep1Phi, "tLep1Phi/F");
  // m_treeMap[m_currentVar]->Branch("tLep1Type", &tLep1Type, "tLep1Type/I");

  // m_treeMap[m_currentVar]->Branch("tLep2Pt", &tLep2Pt, "tLep2Pt/F");
  // m_treeMap[m_currentVar]->Branch("tLep2E", &tLep2E, "tLep2E/F");
  // m_treeMap[m_currentVar]->Branch("tLep2Eta", &tLep2Eta, "tLep2Eta/F");
  // m_treeMap[m_currentVar]->Branch("tLep2Phi", &tLep2Phi, "tLep2Phi/F");
  // m_treeMap[m_currentVar]->Branch("tLep2Type", &tLep2Type, "tLep2Type/I");

  // m_treeMap[m_currentVar]->Branch("tLep3Pt", &tLep3Pt, "tLep3Pt/F");
  // m_treeMap[m_currentVar]->Branch("tLep3E", &tLep3E, "tLep3E/F");
  // m_treeMap[m_currentVar]->Branch("tLep3Eta", &tLep3Eta, "tLep3Eta/F");
  // m_treeMap[m_currentVar]->Branch("tLep3Phi", &tLep3Phi, "tLep3Phi/F");
  // m_treeMap[m_currentVar]->Branch("tLep3Type", &tLep3Type, "tLep3Type/I");

  // m_treeMap[m_currentVar]->Branch("tNeut1Pt", &tNeut1Pt, "tNeut1Pt/F");
  // m_treeMap[m_currentVar]->Branch("tNeut1E", &tNeut1E, "tNeut1E/F");
  // m_treeMap[m_currentVar]->Branch("tNeut1Eta", &tNeut1Eta, "tNeut1Eta/F");
  // m_treeMap[m_currentVar]->Branch("tNeut1Phi", &tNeut1Phi, "tNeut1Phi/F");
  // m_treeMap[m_currentVar]->Branch("tNeut1Type", &tNeut1Type, "tNeut1Type/I");

  // m_treeMap[m_currentVar]->Branch("tNeut2Pt", &tNeut2Pt, "tNeut2Pt/F");
  // m_treeMap[m_currentVar]->Branch("tNeut2E", &tNeut2E, "tNeut2E/F");
  // m_treeMap[m_currentVar]->Branch("tNeut2Eta", &tNeut2Eta, "tNeut2Eta/F");
  // m_treeMap[m_currentVar]->Branch("tNeut2Phi", &tNeut2Phi, "tNeut2Phi/F");
  // m_treeMap[m_currentVar]->Branch("tNeut2Type", &tNeut2Type, "tNeut2Type/I");

  m_treeMap[m_currentVar]->Branch("DRjj", &DRjj, "DRjj/F");
  m_treeMap[m_currentVar]->Branch("DRll", &DRll, "DRll/F");
  // m_treeMap[m_currentVar]->Branch("DRj1l1", &DRj1l1, "DRj1l1/F");
  // m_treeMap[m_currentVar]->Branch("DRj2l2", &DRj2l2, "DRj2l2/F");
  // m_treeMap[m_currentVar]->Branch("DRj1l2", &DRj1l2, "DRj1l2/F");
  // m_treeMap[m_currentVar]->Branch("DRj2l1", &DRj2l1, "DRj2l1/F");

  m_treeMap[m_currentVar]->Branch("pass3lTT", &pass3lTT, "pass3lTT/O");
  m_treeMap[m_currentVar]->Branch("pass0SFOS", &pass0SFOS, "pass0SFOS/O");
  m_treeMap[m_currentVar]->Branch("pass1SFOS", &pass1SFOS, "pass1SFOS/O");
  m_treeMap[m_currentVar]->Branch("passTrig", &passTrig, "passTrig/O");
  m_treeMap[m_currentVar]->Branch("passTrigMatch", &passTrigMatch, "passTrigMatch/O");
  //m_treeMap[m_currentVar]->Branch("isNotBadJet", &isNotBadJet, "isNotBadJet/O");
  m_treeMap[m_currentVar]->Branch("isPV", &isPV, "isPV/O");
  m_treeMap[m_currentVar]->Branch("passSameSign", &passSameSign, "passSameSign/O");
  m_treeMap[m_currentVar]->Branch("pass3Lepton", &pass3Lepton, "pass3Lepton/O");
  m_treeMap[m_currentVar]->Branch("pass2Lepton", &pass2Lepton, "pass2Lepton/O");
  m_treeMap[m_currentVar]->Branch("pass1Lepton", &pass1Lepton, "pass1Lepton/O");
  m_treeMap[m_currentVar]->Branch("pass1ZLepton", &pass1ZLepton, "pass1ZLepton/O");
  m_treeMap[m_currentVar]->Branch("pass1WLepton", &pass1WLepton, "pass1WLepton/O");
  m_treeMap[m_currentVar]->Branch("pass2ZLepton", &pass2ZLepton, "pass2ZLepton/O");
  m_treeMap[m_currentVar]->Branch("pass3ZLepton", &pass3ZLepton, "pass3ZLepton/O");
  m_treeMap[m_currentVar]->Branch("pass3Leptonorigin", &pass3Leptonorigin, "pass3Leptonorigin/O");
  m_treeMap[m_currentVar]->Branch("pass4Lepton", &pass4Lepton, "pass4Lepton/O");
  m_treeMap[m_currentVar]->Branch("pass3LRegion", &pass3LRegion, "pass3LRegion/O");
  m_treeMap[m_currentVar]->Branch("passMET", &passMET, "passMET/O");
  m_treeMap[m_currentVar]->Branch("passNJets", &passNJets, "passNJets/O");
  m_treeMap[m_currentVar]->Branch("passLeptonPt", &passLeptonPt, "passLeptonPt/O");
  m_treeMap[m_currentVar]->Branch("passLeptonPtLow", &passLeptonPtLow, "passLeptonPtLow/O");
  m_treeMap[m_currentVar]->Branch("passJetPt", &passJetPt, "passJetPt/O");
  m_treeMap[m_currentVar]->Branch("passJetEta", &passJetEta, "passJetEta/O");
  m_treeMap[m_currentVar]->Branch("passMjj", &passMjj, "passMjj/O");
  m_treeMap[m_currentVar]->Branch("passWSide", &passWSide, "passWSide/O");
  m_treeMap[m_currentVar]->Branch("passDEtaJJ", &passDEtaJJ, "passDEtaJJ/O");
  m_treeMap[m_currentVar]->Branch("passMll", &passMll, "passMll/O");
  m_treeMap[m_currentVar]->Branch("passZVeto", &passZVeto, "passZVeto/O");
  m_treeMap[m_currentVar]->Branch("passNBtag", &passNBtag, "passNBtag/O");
  m_treeMap[m_currentVar]->Branch("passAuthor", &passAuthor, "passAuthor/O");
  m_treeMap[m_currentVar]->Branch("passPhoton", &passPhoton, "passPhoton/O");

  m_treeMap[m_currentVar]->Branch("passJetLepOverlap", &passJetLepOverlap, "passJetLepOverlap/O");
  m_treeMap[m_currentVar]->Branch("AllJetLepOverlap", &AllJetLepOverlap, "AllJetLepOverlap/I");

  // m_treeMap[m_currentVar]->Branch("passBasics",&passBasics,"passBasics/O");
  m_treeMap[m_currentVar]->Branch("passWWWSR", &passWWWSR, "passWWWSR/O");
  m_treeMap[m_currentVar]->Branch("passWWWSRorigin", &passWWWSRorigin, "passWWWSRorigin/O");
  m_treeMap[m_currentVar]->Branch("passSideBandCR", &passSideBandCR, "passSideBandCR/O");
  m_treeMap[m_currentVar]->Branch("passSideBandCRorigin", &passSideBandCRorigin, "passSideBandCRorigin/O");
  m_treeMap[m_currentVar]->Branch("passBtagCR", &passBtagCR, "passBtagCR/O");
  m_treeMap[m_currentVar]->Branch("passl2jetCR", &passl2jetCR, "passl2jetCR/O");
  m_treeMap[m_currentVar]->Branch("pass3lCR", &pass3lCR, "pass3lCR/O");
  m_treeMap[m_currentVar]->Branch("pass3lVgamma", &pass3lVgamma, "pass3lVgamma/O");
  m_treeMap[m_currentVar]->Branch("pass3lExpCR", &pass3lExpCR, "pass3lExpCR/O");
  m_treeMap[m_currentVar]->Branch("passZWindowCR", &passZWindowCR, "passZWindowCR/O");
  m_treeMap[m_currentVar]->Branch("pass3lSR", &pass3lSR, "pass3lSR/O");
  m_treeMap[m_currentVar]->Branch("pass3lBtag", &pass3lBtag, "pass3lBtag/O");
  m_treeMap[m_currentVar]->Branch("passCFCR", &passCFCR, "passCFCR/O");

  // truth
  m_treeMap[m_currentVar]->Branch("truth_PdgId", &truth_PdgId);
  m_treeMap[m_currentVar]->Branch("Ntruthtau", &Ntruthtau, "Ntruthtau/I");

  m_treeMap[m_currentVar]->Branch("isGoodData", &isGoodData, "isDoodData/I");
  // m_treeMap[m_currentVar]->Branch("Leptons",&Leptons);
  // m_treeMap[m_currentVar]->Branch("Electrons",&Electrons);
  // m_treeMap[m_currentVar]->Branch("Jets",&Jets);

  // m_treeMap[m_currentVar]->Branch("passLepJetPhi",&passLepJetPhi,"passLepJetPhi/O");
  // m_treeMap[m_currentVar]->Branch("passLepbJetPhi",&passLepbJetPhi,"passLepbJetPhi/O");
  // m_treeMap[m_currentVar]->Branch("LepJetPhi",&LepJetPhi,"LepJetPhi/f");
  // m_treeMap[m_currentVar]->Branch("LepMPTPhi",&LepMPTPhi,"LepMPTPhi/f");
}

void MVATree_WWW::Reset() {
  mllSFOS = 0;
  mtll = 0;
  mtlll = 0;
  mtLep1 = 0;
  mtLep2 = 0;
  mtLep3 = 0;
  dEtall = 0;
  etalll = 0;
  dPhill = 0;
  philll = 0;

  vetoMuonPt = 0;
  vetoMuonEta = 0;
  vetoMuonPhi = 0;

  vetoElectronPt = 0;
  vetoElectronEta = 0;
  vetoElectronPhi = 0;

  Jet1Pt = 0.0;
  Jet1M = 0.0;
  Jet1E = 0.0;
  Jet1Eta = 0.0;
  Jet1Phi = 0.0;

  bJet1Pt = 0.0;
  bJet1M = 0.0;
  bJet1E = 0.0;
  bJet1Eta = 0.0;
  bJet1Phi = 0.0;

  Jet2Pt = 0.0;
  Jet2M = 0.0;
  Jet2E = 0.0;
  Jet2Eta = 0.0;
  Jet2Phi = 0.0;

  Jet1nrMuon = 0;
  Jet1nrEl = 0;
  Jet2nrMuon = 0;
  Jet2nrEl = 0;

  Lep1Pt = 0.0;
  Lep1E = 0.0;
  Lep1Eta = 0.0;
  Lep1Phi = 0.0;
  Lep1innerTrackPt = 0.0;

  Lep3Pt = 0.0;
  Lep3E = 0.0;
  Lep3Eta = 0.0;
  Lep3Phi = 0.0;

  Lep1Q = 0.0;
  Lep1Flav = 0.0;
  Lep1isLoose = 0;

  Lep1isNoBL = 0;
  Lep2isNoBL = 0;
  Lep3isNoBL = 0;

  Lep2Q = 0.0;
  Lep2Flav = 0.0;
  Lep2isLoose = 0;

  Lep3Q = 0.0;
  Lep3Flav = 0.0;
  Lep3isLoose = 0;

  Lep1Author = 0;
  Lep2Author = 0;
  Lep3Author = 0;

  LooseMuPt = 0.0;
  LooseMuE = 0.0;
  LooseMuEta = 0.0;
  LooseMuPhi = 0.0;

  LooseElPt = 0.0;
  LooseElE = 0.0;
  LooseElEta = 0.0;
  LooseElPhi = 0.0;

  Lep1PromptWeight = -1.0;
  Lep2PromptWeight = -1.0;
  LepPLVEl = -1.0;
  LepPLVMu = -1.0;

  Lep1ECIDSBDT = 1.0;
  Lep2ECIDSBDT = 1.0;
  LepECIDSBDT = 1.0;

  LepAddAmbiguity = -2;

  passBasics = false;
  passWWWSR = false;
  passWWWSRorigin = false;
  passSideBandCR = false;
  passSideBandCRorigin = false;
  passBtagCR = false;
  passl2jetCR = false;
  pass3lCR = false;
  pass3lVgamma = false;
  pass3lExpCR = false;
  passZWindowCR = false;
  pass3lSR = false;
  pass3lBtag = false;
  passCFCR = false;

  passJetLepOverlap = false;
  AllJetLepOverlap = 0;

  Lep1TopoEtCone20 = -999.;
  Lep1PtCone20 = -999.;
  Lep1PtVarCone20 = -999.;
  Lep1PtVarCone30 = -999.;
  Lep1PtVarCone40 = -999.;
  Lep1PtVarCone20_TightTTVA_pt1000 = -999.;
  Lep1PtVarCone30_TightTTVA_pt1000 = -999.;

  Lep2TopoEtCone20 = -999.;
  Lep2PtCone20 = -999.;
  Lep2PtVarCone20 = -999.;
  Lep2PtVarCone30 = -999.;
  Lep2PtVarCone40 = -999.;
  Lep2PtVarCone20_TightTTVA_pt1000 = -999.;
  Lep2PtVarCone30_TightTTVA_pt1000 = -999.;

  Lep1d0 = -99;

  Lep2d0 = -99;

  Lep2Pt = 0.0;
  Lep2E = 0.0;
  Lep2Eta = 0.0;
  Lep2Phi = 0.0;
  Lep2innerTrackPt = 0.0;

  Lep3Pt = 0.0;
  Lep3E = 0.0;
  Lep3Eta = 0.0;
  Lep3Phi = 0.0;
  Lep3innerTrackPt = 0.0;

  tLep1Pt = -1.0;
  tLep1E = -1.0;
  tLep1Eta = 0.0;
  tLep1Phi = 0.0;
  tLep1Type = 0;

  tLep2Pt = -1.0;
  tLep2E = 0.0;
  tLep2Eta = 0.0;
  tLep2Phi = 0.0;
  tLep2Type = 0;

  tLep3Pt = -1.0;
  tLep3E = 0.0;
  tLep3Eta = 0.0;
  tLep3Phi = 0.0;
  tLep3Type = 0;

  tNeut1Pt = -1.0;
  tNeut1E = -1.0;
  tNeut1Eta = 0.0;
  tNeut1Phi = 0.0;
  tNeut1Type = 0;

  tNeut2Pt = -1.0;
  tNeut2E = 0.0;
  tNeut2Eta = 0.0;
  tNeut2Phi = 0.0;
  tNeut2Type = 0;

  QCDweight = 0;
  CFweight = 0;
  CFStatSys = 0;
  CFwBG = 0;
  Nloose = 0;
  Nlooseel = 0;
  Nloosemu = 0;

  NNoBLel = 0;

  Nloosetau = 0;
  Nmediumtau = 0;
  Ntighttau = 0;
  TauPt = 0.0;
  TauE = 0.0;
  TauEta = 0.0;
  TauPhi = 0.0;
  TaunTracks = 0;

  NAllMu = 0;
  NAllEl = 0;
  NAllTa = 0;

  runNumber = 0;
  eventNumber = 0;
  Nleptons = 0;
  Nbjets = 0;
  Nbjets77 = 0;
  Njets = 0;
  Nel = 0;
  TotalQ = 0;
  Nmu = 0;
  met = 0;
  metSig = 0;
  metSigRatio = 0;

  mpt = 0;
  MT = 0;
  MTtrack = 0;
  metPhi = 0;
  mptPhi = 0;

  weight = 0;
  weightMC = 1;
  weightSys = 1;
  weightJVT = 1;
  weightBtag = 1;
  weightEffSF = 1;
  weightEffReco = 1;
  weightIso = 1;
  weightTTVA = 1;
  weightTrig = 1;

  LepOFQ = 0;
  LepOFFlav = 0;
  LepOFisLoose = 0;
  LepOFisNoBL = 0;
  LepOFPt = 0;
  LepOFE = 0;
  LepOFEta = 0;
  LepOFPhi = 0;

  Mjj = 0;
  MWWW = 0;
  Mll = 0;
  Mllee = 0;
  Mllmm = 0;
  MllSF = 0;
  Mll_forwardelectron = 0;
  forwardElPt = 0;
  forwardElEta = 0;
  Mlll = 0;

  Ptjj = 0;
  PtWWW = 0;
  Ptll = 0;

  Etajj = 0;
  DEtajj = 0;
  EtaWWW = 0;
  Etall = 0;

  Phijj = 0;
  PhiWWW = 0;
  Phill = 0;

  SumEta = 0;
  SumPt = 0;

  DRjj = 0;
  DRll = 0;
  DRj1l1 = 0;
  DRj2l2 = 0;
  DRj1l2 = 0;
  DRj2l1 = 0;

  Jet1NTrkPt500PV = -99;
  Jet1NTrkPt1000PV = -99;
  Jet2NTrkPt500PV = -99;
  Jet2NTrkPt1000PV = -99;

  pass3lTT = false;
  pass0SFOS = false;
  pass1SFOS = false;

  passTrig = false;
  passTrigMatch = false;
  passSameSign = false;
  pass3Lepton = false;
  pass1ZLepton = false;
  pass1WLepton = false;
  pass2ZLepton = false;
  pass3ZLepton = false;
  pass3Leptonorigin = false;
  pass4Lepton = false;
  pass3LRegion = false;
  passMET = false;
  passNJets = false;
  passLeptonPt = false;
  passLeptonPtLow = false;
  passJetPt = false;
  passJetEta = false;
  passMjj = false;
  passWSide = false;
  passDEtaJJ = false;
  passMll = false;
  passZVeto = false;
  passNBtag = false;
  passLepJetPhi = false;
  passAuthor = false;
  passPhoton = false;

  LepJetPhi = -99;
  LepMPTPhi = -99;

  truth_PdgId.clear();
  Ntruthtau = 0;

  Leptons.clear();
  // Electrons.clear();
  Jets.clear();
}

void MVATree_WWW::ReadMVA() {
  // not used
  return;

  //   if (!m_readMVA) return;

  //   BDT = m_reader.EvaluateMVA("reader");
}

void MVATree_WWW::TransformVars() {
  // Not used
  return;
}

void MVATree_WWW::Fill() {
  MVATree::Fill();

  if (m_currentVar == "Nominal" && m_weightSysts->size() > 0) {
    float WeightOrig = weight;

    for (unsigned int i = 0; i < m_weightSysts->size(); i++) {
      string varName = m_weightSysts->at(i).name;
      if (m_treeMap.find(varName) == m_treeMap.end()) {
        TFile *f = wk->getOutputFile("MVATree");
        m_treeMap[varName] = new TTree(varName.c_str(), varName.c_str());
        m_treeMap[varName]->SetDirectory(f);
        SetVariation(varName);
        SetBranches();
        SetVariation("Nominal");
      }

      if (m_treeMap.find(varName) != m_treeMap.end()) {
        weight = WeightOrig * m_weightSysts->at(i).factor;
        m_treeMap[varName]->Fill();
        weight = WeightOrig;
      }
    }
  }
}
