#ifndef CxAODTools__PreEvtSelection_H
#define CxAODTools__PreEvtSelection_H

#include "CxAODTools/ConfigStore.h"
#include "CxAODReader_WZ/DBEvtSelection.h"

class PreEvtSelection : public DBEvtSelection {
 public:
  PreEvtSelection(ConfigStore* config);
  virtual ~PreEvtSelection() noexcept {}

  virtual bool passSelection(SelectionContainers& containers, bool isKinVar);

  virtual bool passPreSelection(SelectionContainers& containers, bool isKinVar);

  //virtual EL::StatusCode writeEventVariables(const xAOD::EventInfo* eventInfoIn, xAOD::EventInfo* eventInfoOut, bool isKinVar,
  //                                           bool isWeightVar, std::string sysName, int rdm_RunNumber,
  //                                           CP::MuonTriggerScaleFactors* trig_sfmuon) override;

 private:
  //Config store
  ConfigStore* m_config;

  bool m_debug;

  //leading and sub-leading leptons
  const xAOD::Electron* el_1;
  const xAOD::Muon* mu_1;
  const xAOD::Electron* el_2;
  const xAOD::Muon* mu_2;

  std::vector<const xAOD::Electron*> _el;
  std::vector<const xAOD::Electron*> _fwdel;
  std::vector<const xAOD::Muon*> _mu;
  std::vector<const xAOD::TauJet*> _ta;

  //Cut variables(configurable via config file)
  // double m_ptAsymCut;
  // double m_deltaYCut;
  // double m_YFiltCut;
  // std::string m_softTerm;
  // std::vector<std::string> m_TriggerList;

  void SetParameters();

  // int passTriggerSelection(const xAOD::EventInfo* evtinfo);
  //int passJetSelection(const xAOD::JetContainer* fatjets);
  // int passJetPreSelection(const xAOD::JetContainer* akt4jets, const xAOD::JetContainer* fatjets, const xAOD::JetContainer* trackjets);
};

#endif
