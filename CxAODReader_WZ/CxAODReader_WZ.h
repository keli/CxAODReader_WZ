#ifndef CxAODReader_WZ_CxAODReader_WZ_H
#define CxAODReader_WZ_CxAODReader_WZ_H

#include <EventLoop/Algorithm.h>
#include <PATInterfaces/CorrectionCode.h>
#include <TFile.h>
#include <TH1.h>
#include <CxAODReader_WZ/MVATree_WWW.h>
#include <xAODRootAccess/Init.h>
#include <chrono>
#include <vector>
#include "CxAODReader/AnalysisReader.h"
#include "CxAODTools_WZ/CommonProperties_WWW.h"
#include "CxAODTools_WZ/EvtWeightVariations_WWW.h"
#include "CxAODTools_WZ/TriggerTool_WWW.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
#include "PathResolver/PathResolver.h"

//#include "TriggerAnalysisInterfaces/ITrigGlobalEfficiencyCorrectionTool.h"

class CxAODReader_WZ : public AnalysisReader {
  MVATree_WWW *m_tree = NULL;  //!

  CP::PileupReweightingTool *m_datareweight_el;  //!
  CP::PileupReweightingTool *m_datareweight_mu;  //!
  EvtWeightVariations_WWW *m_EvtWeightVar_WWW;   //!
  int m_eventCounter;                            //!
  int m_FakesOrNormal;                           //!

 public:
  // this is a standard constructor
  CxAODReader_WZ();
  void FillHist(string CutName);

  //virtual EL::StatusCode histInitialize() override; /**< Initialize output histograms                                   */
  virtual EL::StatusCode fileExecute() override; /**< Put everything that must be executed once for eachx file here   */
  virtual EL::StatusCode finalize() override;    /**< Delete tools and finalize histograms                           */

  // this is needed to distribute the algorithm to the workers
  TFile *file_fakefactor;  //!
  TFile *file_CFrate;      //!
  TFile *file_CFrate_wBG;  //!
  TH2F *h_ff_mu;           //!
  TH2F *h_ff_el;           //!
  TH2F *h_CF_el;           //!
  TH2F *h_CF_el_wBG;       //!

  CP::MuonTriggerScaleFactors *m_muonTrigSF;  //!

 protected:
  EL::StatusCode fill_WZ();

  virtual EL::StatusCode initializeSelection() override;

  /* virtual EL::StatusCode initializeVariations() override; */

  virtual EL::StatusCode initializeSumOfWeights() override;

  virtual EL::StatusCode initializeTools() override;

  /*
   * Clear all internal vectors
   */
  EL::StatusCode clearVectors();

  // this is needed to distribute the algorithm to the workers
  ClassDefOverride(CxAODReader_WZ, 1);
};

#endif
