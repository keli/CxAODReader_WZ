
import ROOT
import math
from WWWColors import getColorForCategory,rainbow,darkRainbow,lightRainbow
#Histos: Array of Histograms [Data,Signal,BGs..]
#Names: Array of Names of the Histograms for Legends
#YAxisScaleFactor: This will scale the YAxis by the given factor so that it looks better. 
#XAxisTitle: XAxisTitle Like Mjj SumPT etc. 
#Syst_Error: Overall Syst Uncertainty, If you want we may change this.
#ExtraText: Extra Tex Uder Luminosity like WWW etc.
def WWWPlot(Histos,YAxisScaleFactor=1.2,XAxisTitle="",Syst_Error=0,ExtraText=[],logy=False,xrange=[],blind=False,Multip=False,NonGEV=False,HistosAQGC=[],NamesAQGC=[],calculateOverflow=False, FitFunc={}):

    # ROOT.gROOT.LoadMacro("AtlasStyle.C") 
    # ROOT.gROOT.LoadMacro("AtlasUtils.C") 


    
    Names=Histos.keys()
    Histos=Histos.values()


    """
    Colors=[ROOT.TColor(4001,0.89, 0.10, 0.11),
            ROOT.TColor(4002,0.22, 0.49, 0.72),
            ROOT.TColor(4003,0.30, 0.69, 0.29),
            ROOT.TColor(4004,0.60, 0.31, 0.64),
            ROOT.TColor(4005,1,.5,0),
            ROOT.TColor(4006,1, 1, 0.2)]

    ColorNumbers=[4001,4002,4003,4004,4005,4006]
    """

    PadRange=0.3
    if blind: PadRange=0
    ROOT.gStyle.SetOptStat(0)

    

    Canvas = ROOT.TCanvas("c","canvas",600,600)


    pad1 = ROOT.TPad("pad1", "pad1", 0, PadRange, 1, 1.0)
    pad1.SetTickx()
    pad1.SetTicky()
    if not blind: pad1.SetBottomMargin(0.02)
    pad1.Draw()

    pad2 = ROOT.TPad("pad2", "pad2", 0, 0, 1, PadRange)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.2)
    pad2.SetTickx()
    pad2.SetTicky()
    pad2.SetGridy(1)
    pad2.Draw()

    pad1.cd()

    ROOT.SetOwnership(pad1,False)
    ROOT.SetOwnership(pad2,False)


    BCHists=[]


    ROOT.SetOwnership(Histos[0],False)
    #Get And Format histograms
    for index in range(1,len(Histos)):
        Histograms = Histos[index].Clone()
        if Multip:
            Histograms.GetXaxis().SetNdivisions(Histograms.GetXaxis().GetNbins(),0,0,ROOT.kFALSE)
        ROOT.SetOwnership(Histograms, False)        
        for index2 in range(1,index):
            Histograms.Add(Histos[index2],1)

        
        """
        Histograms.SetFillColor(ColorNumbers[index-2])
        Histograms.SetMarkerColor(ColorNumbers[index-2])
        Histograms.SetLineColor(ColorNumbers[index-2])
        """
        Histograms.SetFillColor(getColorForCategory(Names[index]))
        Histograms.SetMarkerColor(getColorForCategory(Names[index]))
        Histograms.SetLineColor(getColorForCategory(Names[index],lineColor=False))
        Histograms.SetLineWidth(0)
        Histograms.SetTitle("")
        if Multip or NonGEV:
            Histograms.GetYaxis().SetTitle("Events")
        else:
            Histograms.GetYaxis().SetTitle("Events / "+str(int(Histograms.GetXaxis().GetBinWidth(1)))+" GeV")
        
        labelSize = 0.06
        if blind:
            labelSize=0.04
        offset=0.8
        if blind:
            offset=1.1
        Histograms.GetYaxis().SetLabelSize(labelSize)
        #Histograms.GetYaxis().SetTitleOffset(1.05)
        Histograms.GetYaxis().SetTitleOffset(offset)
        Histograms.GetYaxis().SetTitleSize(labelSize)

        if not blind:
            Histograms.GetXaxis().SetLabelSize(0)
            Histograms.SetMinimum(0)
        if blind and XAxisTitle: 
            Histograms.SetMinimum(0)
            Histograms.GetXaxis().SetTitleOffset(1.4)
            Histograms.GetXaxis().SetTitle(XAxisTitle) 


        BCHists.append(Histograms)
        
    xmin = Histos[0].GetXaxis().GetBinLowEdge(1)
    xmax = Histos[0].GetXaxis().GetBinLowEdge(Histos[0].GetNbinsX()+1)


    if len(xrange)==2:
      edgeList = []
      for bin in range(1,Histos[0].GetNbinsX()+2):
        edgeList.append(Histos[0].GetXaxis().GetBinLowEdge(bin))
      #find bin edge that is closest and round to that
      xmin = min(edgeList, key=lambda x:abs(x-xrange[0])) #/x)
      xmax = min(edgeList, key=lambda x:abs(x-xrange[1])) #/x)
      Histos[0].GetXaxis().SetRangeUser(xmin,xmax)
    if calculateOverflow:
      #determine overflow bin to be used
      epsilon = 0.000001
      newOverflowBin = Histos[0].FindBin(xmax-epsilon)
      oldOverflowBin = Histos[0].GetNbinsX()+2
      for hist in BCHists + HistosAQGC + ([Histos[0]] if not blind else []) :
        sum = 0.
        variance = 0.
        for binNum in range(newOverflowBin,oldOverflowBin+1):
          sum += hist.GetBinContent(binNum)
          variance += hist.GetBinError(binNum)**2
        hist.SetBinContent(newOverflowBin,sum)
        hist.SetBinError(newOverflowBin,ROOT.TMath.Sqrt(variance))



    #find y max



    ymax = 0
    for hist in BCHists+HistosAQGC+[Histos[0]]:
        if hist.GetMaximum() > ymax: ymax = hist.GetMaximum()
    #once max has been found set it to be the same for all histograms
    for hist in BCHists+HistosAQGC:
      hist.SetMaximum(ymax*YAxisScaleFactor)
      if logy: hist.SetMinimum(0.1) 


    if logy: 
        pad1.SetLogy()


    #Draw BG Histograms
    for index in range(len(BCHists)-1,-1,-1):
        DrawOpt="hist"
        if not len(BCHists)-1 == index:
            DrawOpt="same"+DrawOpt
        BCHists[index].GetXaxis().SetRangeUser(xmin,xmax)
        #BCHists[index].GetYaxis().SetRangeUser(0,ymax)
        BCHists[index].Draw(DrawOpt)


    #Draw Data
    Histos[0].SetLineColor(ROOT.kBlack)
    if not blind:
        Histos[0].Draw("same ep")
    Histos[0].GetXaxis().SetRangeUser(xmin,xmax)
    if blind:
      pad1.SetBottomMargin(.15)
      pad1.SetRightMargin(.05)

    doAQGC = False
    if len(NamesAQGC)>0 and len(NamesAQGC)==len(HistosAQGC):
        doAQGC = True


  
    shifty = .0
    shiftx = .0
    legendY1 = 0.6
    textSize =0.04
    if doAQGC: #need bigger legend
        #legendY1 = 0.3
        if len(NamesAQGC)==3:
            shiftx = -.03
            legendY1 = 0.2
            textSize = 0.035
        else:
            shiftx = -.03
            legendY1 = 0.3
            textSize = 0.035
    #mylegend = ROOT.TLegend(0.6, 0.6, 0.88, 0.88)
    #mylegend = ROOT.TLegend(0.6, 0.45, 0.88, 0.88)
    mylegend = ROOT.TLegend(0.45, legendY1-shifty, 0.9, 0.88-shifty)
    ROOT.SetOwnership(mylegend, False)
    mylegend.SetNColumns(2)
    mylegend.SetFillColor(0)
    mylegend.SetBorderSize(0)

    #mylegend.SetTextSize(0.045)
    #mylegend.SetTextSize(0.03)
    mylegend.SetTextSize(textSize)
    if not blind:
        mylegend.AddEntry(Histos[0],Names[0]);
    
    for index in range(len(BCHists)-1,-1,-1):
        mylegend.AddEntry(BCHists[index],Names[index+1],"f")





    #Add Fits
    for FitKey in  FitFunc.keys():
 

        print "Drawing Fit for", FitKey
        FitColor=ROOT.kBlack
        FitText="Background Fit"
        if "Data" in FitKey:
            FitColor=ROOT.kBlue
            FitText="Data Fit"




        FitFunc[FitKey].SetLineColor(FitColor)
        mylegend.AddEntry(FitFunc[FitKey],FitText,"l")

        FitFunc[FitKey].Draw("same")






    #Draw AQGCs
    #assumes aqgc names are like m2k_p10k (fs0 = -2k TeV^-4, fs1 = +10k TeV^-4)"
    lineStyles = [9,7,2]
    if doAQGC:
        count = 0
        for name,hist,color,style in zip(NamesAQGC,HistosAQGC,lightRainbow,lineStyles):
          if count>2: 
            print "Can't fit more than three aQGC points. Not drawing remaining points"
            break #don't draw more than three

          hist.SetLineColor(color)
          hist.SetLineStyle(style)
          hist.SetFillColor(0)
          hist.Draw("same hist")
          fs0name,fs1name = name.split("_")    

          fs0 = ""
          if "m" in fs0name: fs0 = "-"
          fs0name = fs0name.replace("m","")
          fs0name = fs0name.replace("p","")
          fs0name = fs0name.replace("k","")
          fs0 += fs0name
          fs1 = ""
          if "m" in fs1name: fs1 = "-"
          fs1name = fs1name.replace("m","")
          fs1name = fs1name.replace("p","")
          fs1name = fs1name.replace("k","")
          fs1 += fs1name   

          legendName = "#splitline{f_{S,0}/#Lambda^{4}= %s TeV^{-4}}{f_{S,1}/#Lambda^{4}= %s TeV^{-4}}" % (fs0,fs1)
          #add blank entries so that splitlines don't overlap
          mylegend.AddEntry(None,"","")
          mylegend.AddEntry(hist,legendName,"l")
          mylegend.AddEntry(None,"","")
          mylegend.AddEntry(None,"","")
          count+=1




    mylegend.Draw(); 

    lLumi = ROOT.TLatex()
    lLumi.SetNDC(1);
    lLumi.SetTextFont(42);
    textsize = 0.05
    textystep = 0.08
    textystart = 0.75

    if blind: 
      textsize = 0.04
      textystep = 0.06
      textystart = .78
    lLumi.SetTextSize(textsize);
    #lLumi.DrawLatex( 0.14,0.70,"#int L=20.3 fb^{-1}, #sqrt{s}= 8 TeV")
    #I believe this is the latest style
    #lLumi.DrawLatex( 0.14,textystart,"#sqrt{s} = 13 TeV, 80 fb^{-1}")
    lLumi.DrawLatex( 0.14,textystart,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    #lLumi.DrawLatex(0.28,0.80,"Work In Progress")
    #lLumi.DrawLatex(0.14,textystart+textystep,"#font[72]{ATLAS} Work In Progress")
    #lLumi.DrawLatex(0.14,textystart+textystep,"#font[72]{ATLAS} Internal")
    lLumi.DrawLatex(0.14,textystart+textystep,"#font[72]{ATLAS} Internal")
    if len(ExtraText)>0 :
        lLumi.DrawLatex(0.14,textystart-textystep,ExtraText[0])
    if len(ExtraText)>1 :

        # if len(ExtraText[1])<17:
        #     lLumi.DrawLatex(0.615,0.43,"#font[72]{"+ExtraText[1]+"}")
        # else:
        for index in range(1,len(ExtraText)):
            if len(NamesAQGC)>0:
                lLumi.DrawLatex(0.14,textystart-textystep*(index+1),"#font[132]{"+ExtraText[index]+"}")

            else:
                lLumi.SetTextAlign(31)
                lLumi.DrawLatex(0.855,0.55-textystep*(index-1),"#font[132]{"+ExtraText[index]+"}")

    


    #Syst Uncertainty on Normal Hist
    hError = BCHists[len(BCHists)-1].Clone()
    ROOT.SetOwnership(hError, False)
    hError.SetFillColor(ROOT.kBlack)
    hError.SetFillStyle(3354)
    hError.SetMarkerSize(0)

    

    for i in range(0,hError.GetNbinsX()):
      #statistical error on model
      dBinError = hError.GetBinError(i)

      #systematic error can either be specified
      #as a constant fraction of the statistical error
      #or as a histogram
      #the histogram should have 
      #error bars equal to the systematic uncertainty
      try:
        dSysError = Syst_Error.GetBinError(i)
      except:
        dSysError = hError.GetBinContent(i)*Syst_Error
        dBinError = math.sqrt(dBinError*dBinError+dSysError*dSysError)
        hError.SetBinError(i,dBinError);
      
    hError.Draw("sameE2")
    pad1.RedrawAxis()
    


    if not blind:

        pad2.cd()
        Ratio = Histos[0].Clone()
        Ratio.Sumw2()
        ROOT.SetOwnership(Ratio, False)
        Ratio.SetTitle("")
        Ratio.SetLineColor(ROOT.kBlack)
        Ratio.SetMinimum(0.01)  
        Ratio.SetMaximum(1.99) 
        Ratio.SetStats(0)      
        #labelSize = 0.105
        labelSize = 0.125
        Ratio.GetXaxis().SetLabelSize(labelSize) 
        Ratio.GetYaxis().SetLabelSize(labelSize) 
        Ratio.GetYaxis().SetTitle("Data/S+B")
        #Ratio.GetYaxis().SetTitleOffset(1.05)
        Ratio.GetXaxis().SetTitleOffset(1.05)
        #Ratio.GetYaxis().SetTitleOffset(.45)
        Ratio.GetYaxis().SetTitleOffset(.40)
        #Ratio.GetYaxis().SetTitleSize(0.505) 
        Ratio.GetYaxis().SetNdivisions(505)
        Ratio.GetYaxis().SetTitleSize(labelSize) 
        Ratio.GetXaxis().SetTitleSize(labelSize) 
        Ratio.GetXaxis().SetLabelOffset(0.005) 
        pad2.SetBottomMargin(.3)
        if XAxisTitle: Ratio.GetXaxis().SetTitle(XAxisTitle) 
        Ratio.GetXaxis().SetRangeUser(xmin,xmax)
        if Multip:
            Ratio.GetXaxis().SetNdivisions(Ratio.GetXaxis().GetNbins(),0,0,ROOT.kFALSE)
            Ratio.GetXaxis().CenterLabels(ROOT.kTRUE)

        # if log: pad2.SetLogx()
        Ratio.Divide(BCHists[len(BCHists)-1])
        #Ratio.Draw("EP")
        Ratio.Draw("E0P")
        line = ROOT.TF1("line",str(1.),xmin,xmax)
        ROOT.SetOwnership(line,False)
        line.SetLineWidth(3)
        line.SetLineStyle(2)
        line.SetLineColor(1)
        line.Draw("same")


        SystRatio = hError.Clone();
        ROOT.SetOwnership(SystRatio, False)


        SystRatio.Divide(BCHists[len(BCHists)-1]);
        SystRatio.SetFillColor(ROOT.kBlack);
        SystRatio.SetFillStyle(3354);
        SystRatio.SetMarkerSize(0);
        #SystRatio.Draw("SAMEE2");
        SystRatio.Draw("SAMEE2");





    pad1.cd()

#    Canvas.SaveAs("test.png")
    ROOT.SetOwnership(Canvas, False)
    return Canvas
