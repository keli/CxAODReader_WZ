From CAOD to Ntuples:
1.in run/submit-*/data-CxADO , mkdir “user.*.CxAOD.root” and put the CAOD files in it

2.In CxAODReader_WZ/data/framework-read.cfg
	a.Change the corresponding “dataset_dir” ,“submitDir”, “xSectionFile” and “luminosity”  according to the samples(mca/d/e) you are running. 
	b.The xSectionFile (“XSections_13TeV.txt”) can be found in ${your work area}/CxAODOperations_WWW/data/XSections_13TeV.txt
	if you are using new samples, find the info for xSectionFile in AMI.
	c.The yield files (“yields.13TeV_sorted.txt”) can be found in /eos/atlas/atlascerngroupdisk/phys-sm/WWW_FullRun2/CAOD-r01-06/
	if you are using new samples, find the info in CAOD root files, row 2 (init events),3 ( selected events),4 (sum of weight) corresponding to  MetaData_EventCount的1，3，4 bin contt.

3.Go to ‘run’ folder,do: Reader_WZ



Ntuples analysis :
1.In source/CxAODReader_WZ/macros
	a.RunMacro.py -- reads ntuples and do the plotting
	b.MTree.py -- definitions of the CR and SR. Apply cut by specify the corresponding branch.
	c.AnalyzeSet.py -- mode definition.

2.Do: python RunMacro.py

3.The cutflow plots are in macros/Root/

	d.DataSets.py -- path to inputs. Modify ‘DataFolder’ to your ntuples.

